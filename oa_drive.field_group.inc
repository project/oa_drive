<?php
/**
 * @file
 * oa_drive.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function oa_drive_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_droogle|node|oa_space|form';
  $field_group->group_name = 'group_droogle';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'oa_space';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Drive',
    'weight' => '44',
    'children' => array(
      0 => 'field_oa_droogle_id',
      1 => 'field_oa_droogle_secret',
      2 => 'field_oa_droogle_refresh',
      3 => 'field_oa_droogle_inherit',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Drive',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-droogle field-group-fieldset',
        'description' => 'To create these client ID and secret:

<ol><li>Visit <a href="https://cloud.google.com/console">Google Console</a> and create a project to use.</li> <li>Enable the Drive API and the Drive SDK under APIs tab</li><li>Generate client IDs/Secret under the Credentials tab</li><li>Add <site>/droogle_get_google_token to "Redirect URIs" for the new client ID.</li></ol>',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_droogle|node|oa_space|form'] = $field_group;

  return $export;
}
