; Open Atrium Drive Makefile

api = 2
core = 7.x

; Droogle
projects[Droogle][version] = 4.0-beta15
projects[Droogle][subdir] = contrib
projects[Droogle][download][type] = git
projects[Droogle][download][branch] = 7.x-4.x
projects[Droogle][download][revision] = eaf4c83d67949f29e94aef87ab669c757dbb4109

projects[xautoload][version] = 5.1
projects[xautoload][subdir] = contrib

libraries[google-api-php-client][download][type] = get
libraries[google-api-php-client][download][url] = https://github.com/google/google-api-php-client/archive/1.1.2.tar.gz
libraries[google-api-php-client][directory_name] = google-api-php-client-git-version
libraries[google-api-php-client][destination] = libraries
