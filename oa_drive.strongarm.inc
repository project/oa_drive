<?php
/**
 * @file
 * oa_drive.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function oa_drive_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'droogle_og_field_gdrive_client_id';
  $strongarm->value = 'field_oa_droogle_id';
  $export['droogle_og_field_gdrive_client_id'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'droogle_og_field_gdrive_client_secret';
  $strongarm->value = 'field_oa_droogle_secret';
  $export['droogle_og_field_gdrive_client_secret'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'droogle_og_field_gdrive_refresh_token';
  $strongarm->value = 'field_oa_droogle_refresh';
  $export['droogle_og_field_gdrive_refresh_token'] = $strongarm;

  return $export;
}
